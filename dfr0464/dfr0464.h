//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _DFR0464_H
#define _DFR0464_H

#include <stdint.h>


#define DFR0464_i2cRate         200000  // ������� ������ i2c


#define CP1251                  1251
#define CP866                   866
#define SOURCE_CODEPAGE         CP1251  // ��������� ������ �������� ����� (��� ��������������� ���������)

#define DFR0464_CyrillicSupport 0       // ���� �� ������� ��������� � ������� �������� ����������� ������� 


// ��������� ������������� �������
void dfr0464_init(void);
// ��������� ���������� ���������� �������
void dfr0464_backlight_set(uint8_t R, uint8_t G, uint8_t B);
// ��������� ������� �������
void dfr0464_clear(void);
// ��������� ��������� ������� �������
void dfr0464_goto_xy(uint8_t Row, uint8_t Col);
// ��������� ���������������� ������ ������� � ������� ������� �������
void dfr0464_printf(const char *args, ...);
// ��������� �������� ANSI-������ � ������� ������� �������
void dfr0464_puts(char *str);

#endif