//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <delay.h>
#include <pca9633.h>
#include <i2cm.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>


//==============================================================================
// ��������� ��������� �������� �������
//==============================================================================
void pca9633_SetPWMs(uint8_t Channel0, uint8_t Channel1, uint8_t Channel2, uint8_t Channel3)
{
  i2cm_Start(PCA9633_I2C_ADDR, 0, PCA9633_TO);
  
  uint8_t Buff[5];
  Buff[0] = PCA9633_MODE_06_07_Regs | PCA9633_REG_PWM0;
  Buff[1] = Channel0;
  Buff[2] = Channel1;
  Buff[3] = Channel2;
  Buff[4] = Channel3;
  i2cm_WriteBuff(Buff, sizeof(Buff), PCA9633_TO);

  i2cm_Stop(PCA9633_TO);
}
//==============================================================================


//==============================================================================
// ������� ������ �������� � pca9633
//==============================================================================
int8_t pca9633_reg_set(uint8_t Reg, uint8_t Value)
{
  int8_t result = i2cm_Start(PCA9633_I2C_ADDR, 0, PCA9633_TO);
  if (result != I2C_ERR_Ok)
    return result;
  
  uint8_t Buff[2];
  Buff[0] = Reg;
  Buff[1] = Value;
  result = i2cm_WriteBuff(Buff, sizeof(Buff), PCA9633_TO);

  if (result != I2C_ERR_Ok)
    return result;

  return i2cm_Stop(PCA9633_TO);
}
//==============================================================================


//==============================================================================
// ������������� i2c ��� ������ � pca9633
//==============================================================================
int8_t pca9633_init(void)
{
  // Normal mode, ��������� ��������� i2c
  int8_t result = pca9633_reg_set(PCA9633_REG_MODE1, 0);
  if (result != I2C_ERR_Ok)
    return result;
  
  // set LEDs controllable by both PWM and GRPPWM registers
  result = pca9633_reg_set(PCA9633_REG_LEDOUT, 0xFF);
  if (result != I2C_ERR_Ok)
    return result;

  // set MODE2 values
  // 0010 0000 -> 0x20  (DMBLNK to 1, ie blinky mode)
  return pca9633_reg_set(PCA9633_REG_MODE2, 0x00);
}
//==============================================================================
