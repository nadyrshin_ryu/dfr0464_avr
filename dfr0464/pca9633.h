//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _PCF8574_H
#define _PCF8574_H

#include <stdint.h>


#define PCA9633_I2C_ADDR        0x60    //0xC0    // i2c-����� ����������
#define PCA9633_TO              1000    // ������������ �������� ��������



#define PCA9633_MODE_NoIncrement        0x00    // ��� ���������� ������ ��������
#define PCA9633_MODE_00_0C_Regs         0x80    // ��������� ������ �������� ����� ���� ���������    
#define PCA9633_MODE_02_05_Regs         0xA0    // ��������� ������ �������� ����� ��������� ������� ������� 
#define PCA9633_MODE_06_07_Regs         0xC0    // ��������� ������ �������� ����� ��������� ��������� ���������
#define PCA9633_MODE_02_07_Regs         0x00    // ��������� ������ �������� ����� ��������� ������� ������� � ��������� ���������



#define PCA9633_REG_MODE1       0x00    // read/write Mode register 1
#define PCA9633_REG_MODE2       0x01    // read/write Mode register 2
#define PCA9633_REG_PWM0        0x02    // read/write brightness control LED0
#define PCA9633_REG_PWM1        0x03    // read/write brightness control LED1
#define PCA9633_REG_PWM2        0x04    // read/write brightness control LED2
#define PCA9633_REG_PWM3        0x05    // read/write brightness control LED3
#define PCA9633_REG_GRPPWM      0x06    // read/write group duty cycle control
#define PCA9633_REG_GRPFREQ     0x07    // read/write group frequency
#define PCA9633_REG_LEDOUT      0x08    // read/write LED output state
#define PCA9633_REG_SUBADR1     0x09    // read/write I2C-bus subaddress 1
#define PCA9633_REG_SUBADR2     0x0A    // read/write I2C-bus subaddress 2
#define PCA9633_REG_SUBADR3     0x0B    // read/write I2C-bus subaddress 3
#define PCA9633_REG_ALLCALLADR  0x0C    // read/write LED All Call I2C-bus address


// ��������� ��������� �������� �������
void pca9633_SetPWMs(uint8_t Channel0, uint8_t Channel1, uint8_t Channel2, uint8_t Channel3);
// ������������� pca9633
int8_t pca9633_init(void);

#endif