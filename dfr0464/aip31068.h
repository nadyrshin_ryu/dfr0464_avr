//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _AIP31068_H
#define _AIP31068_H


#include <stdint.h>


// ������� ����������
#define AIP31068_ROWS                   2       // ���-�� �����
#define AIP31068_COLS                   16      // ���-�� ��������
#define AIP31068_Font5x10               0       // � ��������� 1-�������� �������� ����� 5�10

// i2c-����� ���������� aip31068
#define AIP31068_ADDR                   0x3E    // 7-������ i2c-����� aip31068
#define AIP31068_TO                     1000    // ������������ �������� ��������

// ������� ����������� �������
#define AIP31068_CMD_ClearScreen        0x01
#define AIP31068_CMD_ReturnHome         0x02
#define AIP31068_CMD_EntryModeSet       0x04
#define AIP31068_CMD_DisplaySwitch      0x08
#define AIP31068_CMD_CursorShift        0x10
#define AIP31068_CMD_FunctionSet        0x20
#define AIP31068_CMD_SetCgramAddr       0x40    // ��������� ������ � ������ ��������� ��������
#define AIP31068_CMD_SetDdramAddr       0x80


// ��������� ������������� ������� �� ����������� aip31068
void aip31068_init(void);
// ��������� ������ �������
void aip31068_write_cmd(uint8_t Data);
// ��������� ������ ������� ����
void aip31068_write_data(uint8_t *pData, uint16_t len);
// ��������� ������ � CGRAM-������ ������ ���������� �������
void aip31068_LoadSymbol(uint8_t SymbolNumber, uint8_t *pChar);


#endif