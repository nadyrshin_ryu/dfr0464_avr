//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <ioavr.h>
#include <inavr.h>
#include <stdio.h>
#include <delay.h>
#include <dfr0464.h>
#include <rainbow.h>
#include <main.h>


tRGB BG_color;


//==============================================================================
//
//==============================================================================
void main()
{
  dfr0464_init();
  
  dfr0464_goto_xy(0, 0);
  dfr0464_printf("  �����������\n  � ���������");
  //dfr0464_printf("  Electronics\n   in focus");
  /*
  dfr0464_backlight_set(255, 255, 255);

  while (1)
  {
    dfr0464_goto_xy(0, 0);
    dfr0464_printf("I2C-display DEMO\r\nUpdate time test");
    delay_ms(100);
  }
  */
  while (1)
  {
    rainbow_tick(&BG_color);
    dfr0464_backlight_set(BG_color.R, BG_color.G, BG_color.B);
    delay_ms(50);
  }
}
//==============================================================================
