//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _RAINBOW_H
#define _RAINBOW_H


// ��������� �������� ������
#define Rainbow_ColorLength             30
#define Rainbow_FadeStep                8


typedef struct
{
  uint8_t G;
  uint8_t R;
  uint8_t B;
} tRGB;


// ������ ������
void rainbow_tick(tRGB *pRGB);


#endif