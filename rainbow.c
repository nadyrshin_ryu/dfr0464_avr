//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stdint.h>
#include <rainbow.h>


// �������� ����� ������
const tRGB RainbowColors[] =
{
  {0,   255, 0},        // �������
  {165, 255, 0},        // ���������
  {255, 255, 0},        // Ƹ����
  {255, 0,   0},        // ������
  {255, 0,   255},      // �������
  {0,   0,   255},      // �����
  {0,   255, 255}       // ����������
};

// �������� ������ ����������
static uint8_t ColorIdx = 0;
static uint16_t StepCounter = 0;


//==============================================================================
// ��������� � �������� ����� �������� �������� *source � �������� *desc
//==============================================================================
void StepChange(uint8_t *desc, uint8_t *source, uint8_t Step)
{
  if (*desc < *source)
  {
    if ((0xFF - *desc) < Step)
      *desc = 0xFF;
    else
      *desc += Step;
  }
    
  if (*desc > *source)
  {
    if (*desc > Step)
      *desc -= Step;
    else
      *desc = 0;
  }
}       
//==============================================================================


//==============================================================================
// ��������� � �������� ����� ����������� ���� pLEDsource � pLEDdesc
//==============================================================================
void StepChangeColor(tRGB *pLEDdesc, tRGB *pLEDsource, uint8_t Step)
{
  StepChange(&(pLEDdesc->R), &(pLEDsource->R), Step);
  StepChange(&(pLEDdesc->G), &(pLEDsource->G), Step);
  StepChange(&(pLEDdesc->B), &(pLEDsource->B), Step);
}
//==============================================================================


//==============================================================================
// ������ ������
//==============================================================================
void rainbow_tick(tRGB *pRGB)
{
  // ���� ������ ������ ����   
  if (++StepCounter > Rainbow_ColorLength)
  {
    StepCounter = 0;

    // ����� �����
    if (++ColorIdx >= 7)
      ColorIdx = 0;
  }
        
  // ������� ����� ����� 0-�� ������� � ����� �� ����� Rainbow_FadeStep
  StepChangeColor(pRGB, (tRGB *) &(RainbowColors[ColorIdx]), Rainbow_FadeStep);
}
//==============================================================================
